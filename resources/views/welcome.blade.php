
<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <link rel="shortcut icon" type="image/x-icon" href="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRIOWJS6XwPCGF7xrbgUnKKkoJjK3IMa9v5j0Lz8qo89i3oxKgB" />

    <title> Pages and Screens </title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- icons -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">

    <!-- Styles -->
    <style>
    html, body {
        background-color: #fff;
        color: #636b6f;
        font-family: 'Raleway', sans-serif;
        font-weight: 100;
        height: 100vh;
        margin: 0;
    }

    .full-height {
        height: 100vh;
    }

    .flex-center {
        align-items: center;
        display: flex;
        justify-content: center;
    }

    .position-ref {
        position: relative;
    }

    .top-right {
        position: absolute;
        right: 10px;
        top: 18px;
    }

    .content {
        text-align: center;
    }

    .links > a {
        color: #636b6f;
        padding: 0 25px;
        font-size: 12px;
        font-weight: 600;
        letter-spacing: .1rem;
        text-decoration: none;
        text-transform: uppercase;
    }


    .title {
        font-size: 84px;
    }


    @media (max-width: 800px) {
        .title {
            font-size: 195%;
        }
    }

    .m-b-md {
        margin-bottom: 30px;
    }
</style>
</head>
<body>
    <div class="flex-center position-ref full-height">
        @if (Route::has('login'))
        <div class="top-right links">
            @auth
            <a href="{{ url('/home') }}"><i class="fas fa-home"></i>Home</a>
            @else
            <a href="{{ route('login') }}"><i class="fas fa-sign-in-alt"></i>Login</a>
            <a href="{{ route('register') }}"><i class="fas fa-user-edit"></i>Register</a>
            @endauth
        </div>
        @endif

        <div class="content">
            <div class="title m-b-md">
                <i class="fas fa-book"></i>  Pages and Screens   <i class="fas fa-film"></i>
            </div>

            <div class="links">
                <a href="{{ route('about') }}">About Us</a>
            </div>
        </div>
    </div>
</body>
</html>
