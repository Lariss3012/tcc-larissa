@php app()->setLocale(Auth::user()->lang); @endphp

@extends('layouts.app')

@section('content')

<style type="text/css">
h3{
    margin-bottom: 40px;
}
.book{
    display: none;
}
.movie{
    display: none;
}
.user{
    display: none;
}
.group{
    display: none;
}
.tag{
    display: none;
}
</style>

<script type="text/javascript">
    $(document).ready(function(){
        $("#books").click(function(){
            $(".book").show();
            $(".movie").hide();
            $(".tag").hide();
            $(".user").hide();
            $(".group").hide();
        });
        $("#movies").click(function(){
            $(".book").hide();
            $(".tag").hide();
            $(".user").hide();
            $(".group").hide();
            $(".movie").show();
        });
        $("#tags").click(function(){
            $(".book").hide();
            $(".movie").hide();
            $(".user").hide();
            $(".group").hide();
            $(".tag").show();
        });
        $("#users").click(function(){
            $(".book").hide();
            $(".movie").hide();
            $(".tag").hide();
            $(".group").hide();
            $(".user").show();
        });
        $("#groups").click(function(){
            $(".book").hide();
            $(".movie").hide();
            $(".tag").hide();
            $(".user").hide();
            $(".group").show();
        });

    });
</script>

<div class="container">
    <nav aria-label="...">
      <ul class="pagination">
        <li class="page-item">
            <a class="page-link" id="tags" href="#">Tags</a>
        </li>
        <li class="page-item">
            <a class="page-link" id="users" href="#">{{__('messages.users')}}</a>
        </li>
        <li class="page-item">
            <a class="page-link" id="groups" href="#">{{__('messages.groups')}}</a>
        </li>
        <li class="page-item">
            <a class="page-link" id="books" href="#">{{__('messages.books')}}</a>
        </li>
        <li class="page-item ">
          <a class="page-link" id="movies" href="#"> {{__('messages.movies')}}</a>
      </li>


  </ul>
</nav>


<div class="container book" id="book">
    <div class="row">
        <div class="col-sm-10">
            <div class="card">
                <div class="card-header"><h5>{{__('messages.searchbook')}} <i class="fa fa-book" aria-hidden="true"></i></h5></div>
                <div class="card-body">
                    <form class="form-inline md-form form-sm" method="get" action="{{route('searchbook')}}">
                        <input class="form-control form-control-md mr-3 w-75" type="text" aria-label="Search" name="book" required>
                        <button type="submit" class="btn btn-primary"><i class="fa fa-search" aria-hidden="true"></i> {{__('messages.search')}}</button>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container movie" id="movie">
    <div class="row">
        <div class="col-sm-10">
            <div class="card">
                <div class="card-header"><h5>{{__('messages.searchmovie')}} <i class="fa fa-film" aria-hidden="true"></i></h5></div>
                <div class="card-body">
                    <form class="form-inline md-form form-sm" method="get" action="{{route('searchmovie')}}">
                        <input class="form-control form-control-md mr-3 w-75" type="text" aria-label="Search" name="movie" required>
                        <button type="submit" class="btn btn-primary"><i class="fa fa-search" aria-hidden="true"></i> {{__('messages.search')}}</button>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container tag" id="tag">
    <div class="row">
        <div class="col-sm-10">
            <div class="card">
                <div class="card-header"><h5> {{__('messages.searchtag')}} <i class="fa fa-tags" aria-hidden="true"></i></h5></div>
                <div class="card-body">

                    <form class="form-inline md-form form-sm" method="get" action="{{route('searchtag')}}">
                        <input class="form-control form-control-md mr-3 w-75" type="text"  aria-label="Search" name="tag">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-search" aria-hidden="true"></i> {{__('messages.search')}}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container user" id="user">
    <div class="row">
        <div class="col-sm-10">
            <div class="card">
                <div class="card-header"><h5>{{__('messages.searchuser')}} <i class="fa fa-user" aria-hidden="true"></i></h5></div>
                <div class="card-body">

                    <form class="form-inline md-form form-sm" method="get" action="{{route('searchuser')}}">
                        <input class="form-control form-control-md mr-3 w-75" type="text" aria-label="Search" name="user">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-search" aria-hidden="true"></i> {{__('messages.search')}}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container group" id="group">
    <div class="row">
        <div class="col-sm-10">
            <div class="card">
                <div class="card-header"><h5>{{__('messages.searchgroup')}} <i class="fa fa-user-friends" aria-hidden="true"></i></h5></div>
                <div class="card-body">

                    <form class="form-inline md-form form-sm" method="get" action="{{route('searchgroup')}}">
                        <input class="form-control form-control-md mr-3 w-75" type="text"  aria-label="Search" name="group">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-search" aria-hidden="true"></i> {{__('messages.search')}}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
