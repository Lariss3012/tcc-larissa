@php app()->setLocale(Auth::user()->lang); @endphp

@extends('layouts.app')

@section('content')


<style type="text/css">
.list-group-item{
  margin-bottom: 10px;
  margin-top: 10px;
  padding: 20px;

}
form{
  padding: 20px;
}
button{
  margin-left: 20px;
}
</style>

<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-10">
      <form class="form-inline md-form form-sm" method="get" action="{{route('searchmovie')}}">
        <input class="form-control" type="text" aria-label="Search" name="movie">
        <button type="submit" class="btn btn-primary"><i class="fa fa-search" aria-hidden="true"></i> {{__('messages.search')}}</button>
      </form>
    </div>
  </div>

  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-10">
        @if(isset($results['Error']) && count($movies_bd) == 0)
          <h3>{{__('messages.notfound')}}</h3>
          @else
          <h3>{{__('messages.selectmovie')}} </h3>

        <div class="list-group">

          <a href="modal{{$results['imdbID']}}" class="list-group-item list-group-item-action flex-column align-items-start" data-toggle="modal" data-target="#modal{{$results['imdbID']}}">
            <div class="d-flex w-100 justify-content-between">
              <h5 class="mb-1">{{$results['Title']}}</h5>
              <small>@if(array_key_exists('Released',$results))
                {{$results['Released']}}
              @endif</small>
            </div>
            <p class="mb-1">@if(array_key_exists('Plot',$results))
              {{__('messages.desc')}} {{$results['Plot']}}
            @endif</p>
            <small>@if(array_key_exists('Director',$results))
              {{__('messages.director')}}  {{$results['Director']}} <br>
              @endif
              @if(array_key_exists('Writer',$results))
              {{__('messages.writers')}} {{$results['Writer']}}  <br>
              @endif
              @if(array_key_exists('Production',$results))
              {{__('messages.producer')}} {{$results['Production']}} <br>
              @endif
            </small>
          </a>

          <!-- Modal -->
          <div class="modal fade" id="modal{{$results['imdbID']}}" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLongTitle">{{$results['Title']}}</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                  <form action="{{route('movie.adding', ['movie' => $results['imdbID']])}}" method="post">
                    @csrf
                    <div class="form-group">
                      <h5>{{__('messages.conclusiondatemovie')}}</h5>
                     <input id="input-date" type="date" class="form-control datepicker" name="date">
                    </div>
                    <div class="form-group">
                      <h5> {{__('messages.liked')}}</h5>
                      <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="liked" value="1"  required="true">
                        <label class="form-check-label" for="liked"><i class="fas fa-thumbs-up"></i></label>
                      </div>
                      <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="liked" value="0" >
                        <label class="form-check-label" for="liked"><i class="fas fa-thumbs-down"></i></label>
                      </div>
                      <h5> {{__('messages.privaterecord')}} </h5>
                      <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="private" value="1" required="true">
                        <label class="form-check-label" for="private">{{__('messages.yes')}}</label>
                      </div>
                      <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="private" value="0">
                        <label class="form-check-label" for="private">{{__('messages.no')}}</label>
                      </div>
                    </div>
                    <div class="form-group">
                <label for="text"><small>Tags</small></label>
                <div class="col-sm-8">
                        <input type="text" id="input-tags" name="tags">
                    <script>
                        $('.modal-body #input-tags').selectize({
                            delimiter: ', ',
                            persist: false,
                            create: function(input) {
                                return {
                                    value: input,
                                    text: input
                                }
                            }
                        });

                    </script>
                </div>
            </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('messages.close')}}</button>
                      <button type="submit" class="btn btn-primary">{{__('messages.submit')}} </button>
                    </div>
                  </form>
                </div>

              </div>
            </div>
          </div>


          @if(count($movies_bd))
          @foreach($movies_bd as $movie)
            @if($movie->movie_id != $results['imdbID'])
          <a href="modal{{$movie->movie_id}}" class="list-group-item list-group-item-action flex-column align-items-start" data-toggle="modal" data-target="#modal{{$movie->movie_id}}">
            <div class="d-flex w-100 justify-content-between">
              <h5 class="mb-1">{{$movie->title}}</h5>
              <small>@if(array_key_exists('publication_date',$movie))
                {{$movie->publication_date}}
              @endif</small>
            </div>
            <p class="mb-1">@if(array_key_exists('description',$movie))
              {{__('messages.desc')}} {{$movie->description}}
            @endif</p>
            <small>@if(array_key_exists('director',$movie))
              {{__('messages.director')}}  {{$movie->director}} <br>
              @endif
              @if(array_key_exists('screenwriter',$movie))
             {{__('messages.writers')}} {{$movie->screenwriter}}  <br>
              @endif
              @if(array_key_exists('producer',$movie))
              {{__('messages.producer')}} {{$movie->producer}} <br>
              @endif
            </small>
          </a>

          <!-- Modal -->
          <div class="modal fade" id="modal{{$movie->movie_id}}" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLongTitle">{{$movie->title}}</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                  <form action="{{route('movie.adding', ['movie' => $movie->movie_id])}}" method="post">
                    @csrf
                    <div class="form-group">
                      <h5>{{__('messages.conclusiondatemovie')}}</h5>
                      <input id="input-date" type="date" class="form-control datepicker" name="date">
                    </div>
                    <div class="form-group">
                      <h5> {{__('messages.liked')}} </h5>
                      <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="liked" value="1"  required="true">
                        <label class="form-check-label" for="liked"><i class="fas fa-thumbs-up"></i></label>
                      </div>
                      <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="liked" value="0" >
                        <label class="form-check-label" for="liked"><i class="fas fa-thumbs-down"></i></label>
                      </div>
                      <h5> {{__('messages.privaterecord')}} </h5>
                      <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="private" value="1" required="true">
                        <label class="form-check-label" for="private">{{__('messages.yes')}}</label>
                      </div>
                      <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="private" value="0">
                        <label class="form-check-label" for="private">{{__('messages.no')}}</label>
                      </div>
                    </div>
                    <div class="form-group">
                <label for="text"><small>Tags</small></label>
                <div class="col-sm-8">
                        <input type="text" id="input-tags" name="tags">
                    <script>
                        $('.modal-body #input-tags').selectize({
                            delimiter: ', ',
                            persist: false,
                            create: function(input) {
                                return {
                                    value: input,
                                    text: input
                                }
                            }
                        });

                    </script>
                </div>
            </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('messages.close')}}</button>
                      <button type="submit" class="btn btn-primary">{{__('messages.submit')}}</button>
                    </div>
                  </form>
                </div>

              </div>
            </div>
          </div>
            @endif
          @endforeach
          @endif

            @endif
        </div>
      </div>
    </div>
  </div>
</body>
</html>


@endsection
