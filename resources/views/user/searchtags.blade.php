@php app()->setLocale(Auth::user()->lang); @endphp

@php
use App\User;
@endphp

@extends('layouts.app')

@section('content')


<style type="text/css">
.list-group-item{
  margin-bottom: 10px;
  margin-top: 10px;
  padding: 20px;
}
form{
  padding: 20px;
}
button{
  margin-left: 20px;
}
.card{
    margin-bottom: 10px;
    margin-top: 10px;
}
.record{
    display: none;
}
.post{
    display: none;
}
</style>

<script type="text/javascript">
    $(document).ready(function(){
        $("#posts").click(function(){
            $(".post").show();
            $(".record").hide();
        });
        $("#records").click(function(){
            $(".post").hide();
            $(".record").show();
        });
    });
</script>


<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-10">
      <form class="form-inline md-form form-sm" method="get" action="{{route('searchtag')}}">
        <input class="form-control" type="text" aria-label="Search" name="tag">
        <button type="submit" class="btn btn-primary"><i class="fa fa-search" aria-hidden="true"></i> Search</button>
    </form>
</div>
</div>

<div class="container">
    <nav aria-label="...">
      <ul class="pagination">
        <li class="page-item"><a class="page-link" id="posts" href="#">Posts</a></li>
        <li class="page-item ">
          <a class="page-link" id="records" href="#"> {{__('messages.records')}}</a>
      </li>
  </ul>
</nav>

<div class="row">
    <div class="col-md-12">
        <div class="list-group post">
            <h5>Posts</h5>
            @if(count($posts))
            @foreach($posts as $p)
            @php $user = DB::table('users')->where('user_id', $p->user_id)->first(); @endphp
            <div class="card" >
              <div class="card-body">
                <div class="row">
                    <div class="col-md-8">
                        <h5 class="card-title">{{$user->username}}</h5>
                    </div>
                    <div class="col-md-4">
                        <small class="card-subtitle mb-2 text-muted">{{__('messages.created')}}: {{ date('d/m/Y', strtotime($p->created_at)) }}</small>
                    </div>
                </div>
                <p class="card-text">{!!$p->content!!}</p>
                <small>{{$p->tags}}</small><br>
            </div>
        </div>
        @endforeach
        @endif
    </div>
</div>



<div class="col-md-12">
    <div class="list-group record">
        <h5>{{__('messages.records')}}</h5>
        @if(count($records))
        @foreach($records as $r)
        @php $user = DB::table('users')->where('user_id', $r->user_id)->first(); @endphp
        <div class="list-group-item list-group-item-action flex-column align-items-start">
            <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1">
                    @if(isset($r->book_name))
                    {{$r->book_name}}
                    @elseif(isset($r->movie_name))
                    {{$r->movie_name}}
                    @endif</h5>
                <p>
                   {{$user->username}}
                </p>
                <small>
                 <div class="row">
                     <div class="col-md-2">
                        @if($r->liked)
                        <i class="fa fa-thumbs-up" aria-hidden="true"></i>
                        @else
                        <i class="fa fa-thumbs-down" aria-hidden="true"></i>
                        @endif
                    </div>
                    <div class="col-md-10">
                        @if(array_key_exists('conclusion_date',$r))
                        {{ date('d/m/Y', strtotime($r->conclusion_date)) }}
                        @endif
                    </div>
                </div>
            </small>
        </div>
    </div>

    @endforeach
    @endif
</div>
</div>
</div>
</div>
</body>
</html>


@endsection

