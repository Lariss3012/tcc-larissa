@php app()->setLocale(Auth::user()->lang); @endphp

@extends('layouts.app')

@section('content')

<style type="text/css">

.card{

    margin-bottom: 32px;
}
.card-header{

    margin-bottom: 32px;
}
td{
    padding: 10px;
}

</style>

<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card">

        <div class="card-header">{{__('messages.yourfriends')}}</div>
        <table>
          <thead>
            <tr>
              <th>{{__('messages.name')}}</th>
              <th>{{__('messages.username')}}</th>
            </tr>
          </thead>
          <tbody>


            @foreach($list_friend as $f)
            <tr>
              <td><a href="{{ route('friend.profile', ['id' => $f]) }}">{{ $f->name }}</a></td>
              <td>{{ $f->username }}</td>
              <td><a href="{{route('friend.removing', ['id' => $f])}}"><button class="btn btn-primary"><i class="fas fa-user-minus"></i>  {{__('messages.unfollow')}}</button></a></td>
            </tr>
            @endforeach


          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</div>

<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card">

        <div class="card-header">{{__('messages.yourfolows')}}</div>
        <table>
          <thead>
            <tr>
              <th>{{__('messages.name')}}</th>
              <th>{{__('messages.username')}}</th>
            </tr>
          </thead>
          <tbody>


            @foreach($list_follows as $f)
            <tr>
              <td><a href="{{ route('friend.profile', ['id' => $f]) }}">{{ $f->name }}</a></td>
              <td>{{ $f->username }}</td>
            </tr>
            @endforeach


          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

@endsection
