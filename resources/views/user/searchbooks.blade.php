@php app()->setLocale(Auth::user()->lang); @endphp

@php
use App\User;
@endphp

@extends('layouts.app')

@section('content')


<style type="text/css">
.list-group-item{
    margin-bottom: 10px;
    margin-top: 10px;
    padding: 20px;
}
form{
    padding: 20px;
}
button{
    margin-left: 20px;
}
</style>

<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-10">
        <form class="form-inline md-form form-sm" method="get" action="{{route('searchbook')}}">
            <input class="form-control" type="text" aria-label="Search" name="book">
            <button type="submit" class="btn btn-primary"><i class="fa fa-search" aria-hidden="true"></i> {{__('messages.search')}}</button>
        </form>
    </div>
</div>

<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-10">
        <h3>{{__('messages.selectbook')}} </h3>

        <div class="list-group">
            @if(isset($results))
            @foreach($results['items'] as $val)

            <a href="modal{{$val['id']}}" class="list-group-item list-group-item-action flex-column align-items-start" data-toggle="modal" data-target="#modal{{$val['id']}}">
                <div class="d-flex w-100 justify-content-between">
                  <h5 class="mb-1">{{$val['volumeInfo']['title']}}</h5>
                  <small>@if(array_key_exists('publishedDate',$val['volumeInfo']))
                      {{$val['volumeInfo']['publishedDate']}}
                  @endif</small>
              </div>
              <p class="mb-1">@if(array_key_exists('description',$val['volumeInfo']))
                {{__('messages.desc')}} {{$val['volumeInfo']['description']}}
            @endif</p>
            <small>@if(array_key_exists('authors',$val['volumeInfo']))
                {{__('messages.authors')}} {{@implode(", ", $val['volumeInfo']['authors'])}} <br>
                @endif
                @if(array_key_exists('publisher',$val['volumeInfo']))
                {{__('messages.editor')}} {{$val['volumeInfo']['publisher']}}

            @endif</small>
        </a>

        <!-- Modal -->
        <div class="modal fade" id="modal{{$val['id']}}" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">{{$val['volumeInfo']['title']}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
              </button>
          </div>
          <div class="modal-body">
            <form action="{{route('book.adding', ['book' => $val['id']])}}" method="post">
                @csrf
                <div class="form-group">
                    <h5>{{__('messages.conclusiondatebook')}}</h5>
                    <input id="input-date" type="date" class="form-control datepicker" name="date">

                </div>
                <div class="form-group">
                    <h5> {{__('messages.liked')}} </h5>
                    <div class="form-check form-check-inline">
                      <input class="form-check-input" type="radio" name="liked" value="1"  required="true">
                      <label class="form-check-label" for="liked"><i class="fas fa-thumbs-up"></i></label>
                  </div>
                  <div class="form-check form-check-inline">
                      <input class="form-check-input" type="radio" name="liked" value="0" >
                      <label class="form-check-label" for="liked"><i class="fas fa-thumbs-down"></i></label>
                  </div>
                  <h5> {{__('messages.privaterecord')}} </h5>
                  <div class="form-check form-check-inline">
                      <input class="form-check-input" type="radio" name="private" value="1" required="true">
                      <label class="form-check-label" for="private">{{__('messages.yes')}}</label>
                  </div>
                  <div class="form-check form-check-inline">
                      <input class="form-check-input" type="radio" name="private" value="0">
                      <label class="form-check-label" for="private">{{__('messages.no')}}</label>
                  </div>

              </div>
              <div class="form-group">
                <label for="text"><small>Tags</small></label>
                <div class="col-sm-8">
                        <input type="text" id="input-tags" name="tags">
                    <script>
                        $('.modal-body #input-tags').selectize({
                            delimiter: ', ',
                            persist: false,
                            create: function(input) {
                                return {
                                    value: input,
                                    text: input
                                }
                            }
                        });

                    </script>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('messages.close')}}</button>
                <button type="submit" class="btn btn-primary">{{__('messages.submit')}} </button>
            </div>
        </form>
    </div>

</div>
</div>
</div>


@endforeach
@endif
</div>
</div>
</div>
</div>
</body>
</html>


@endsection
