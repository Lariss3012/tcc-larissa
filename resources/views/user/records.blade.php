@php app()->setLocale(Auth::user()->lang); @endphp

@extends('layouts.app')

@section('content')

<div class="alert alert-primary alert-dismissible fade show" role="alert">
  {{__('messages.please')}}<br>
  <a href="{{__('messages.linksats')}}"> {{__('messages.sendsats')}} <a>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>

<style type="text/css">
.list-group-item{
    margin-bottom: 10px;
    margin-top: 10px;
    padding: 20px;
}
form{
    padding: 20px;
}
button{
    margin-left: 20px;
}
.movie{
    display: none;
}
.book{
    display: none;
}
</style>

<div class="container">
    <nav aria-label="...">
      <ul class="pagination">
        <li class="page-item"><a class="page-link" id="books" href="#">{{__('messages.books')}}</a></li>
        <li class="page-item ">
          <a class="page-link" id="movies" href="#"> {{__('messages.movies')}}</a>
      </li>
  </ul>
</nav>

<script type="text/javascript">
    $(document).ready(function(){
        $("#books").click(function(){
            $(".book").show();
            $(".movie").hide();
        });
        $("#movies").click(function(){
            $(".book").hide();
            $(".movie").show();
        });
    });
</script>

<div class="row">
    <div class="col-md-12">
        <div class="list-group book">
            <h5>{{__('messages.booksrecords')}}</h5>
            @if(isset($records_books))
            @foreach($records_books as $val)
            <div class="list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-between">
                    <h5 class="mb-1">{{$val->book_name}}</h5>

                    <small>
                        <div class="row">
                            <div class="col-md-12 ">
                                @if($val->user_id == Auth::user()->user_id)
                                <a href="{{route('record.deleting', ['record' => $val->record_id])}}"><i class="fa fa-trash" aria-hidden="true"></i> Excluir</a>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                @if($val->liked)
                                <i class="fa fa-thumbs-up" aria-hidden="true"></i>
                                @else
                                <i class="fa fa-thumbs-down" aria-hidden="true"></i>
                                @endif
                            </div>
                            <div class="col-md-2">
                                @if($val->private)
                                <i class="fa fa-lock" aria-hidden="true"></i>
                                @endif
                            </div>
                            <div class="col-md-8">
                                @if(array_key_exists('conclusion_date',$val))
                                {{ date('d/m/Y', strtotime($val->conclusion_date)) }}
                                @endif
                            </div>
                        </div>
                    </small>
                </div>
                <small class="mb-1">{{$val->tags}}</small>
            </div>
            @endforeach
            @endif
        </div>
    </div>

    <div class="col-md-12">
      <div class="list-group movie">
        <h5>{{__('messages.moviesrecords')}}</h5>
        @if(isset($records_movies))
        @foreach($records_movies as $val)
        <div class="list-group-item list-group-item-action flex-column align-items-start">
            <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1">{{$val->movie_name}}</h5>
                <small>
                    <div class="row">
                        <div class="col-md-12 ">
                            @if($val->user_id == Auth::user()->user_id)
                            <a href="{{route('record.deleting', ['record' => $val->record_id])}}"><i class="fa fa-trash" aria-hidden="true"></i> {{__('messages.delete')}}</a>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                     <div class="col-md-2">
                        @if($val->liked)
                        <i class="fa fa-thumbs-up" aria-hidden="true"></i>
                        @else
                        <i class="fa fa-thumbs-down" aria-hidden="true"></i>
                        @endif
                    </div>
                    <div class="col-md-2">
                        @if($val->private)
                        <i class="fa fa-lock" aria-hidden="true"></i>
                        @endif
                    </div>
                    <div class="col-md-8">
                        @if(array_key_exists('conclusion_date',$val))
                        {{ date('d/m/Y', strtotime($val->conclusion_date)) }}
                        @endif
                    </div>
                </div>
            </small>
        </div>
         <small class="mb-1">{{$val->tags}}</small>
    </div>
    @endforeach
    @endif
</div>
</div>
</div>
</div>
@endsection
