@php app()->setLocale(Auth::user()->lang); @endphp

@php
use App\User;
@endphp

@extends('layouts.app')

@section('content')

<style type="text/css">
.list-group-item{
  margin-bottom: 10px;
  margin-top: 10px;
  padding: 20px;
}
form{
  padding: 20px;
}
button{
  margin-left: 20px;
}
</style>


<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-10">
      <form class="form-inline md-form form-sm" method="get" action="{{route('searchuser')}}">
        <input class="form-control" type="text" aria-label="Search" name="user">
        <button type="submit" class="btn btn-primary"><i class="fa fa-search" aria-hidden="true"></i> {{__('messages.search')}}</button>
      </form>
    </div>
  </div>

  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-10">
        <h3>{{__('messages.users')}} </h3>

        <div class="list-group">
          @if(count($users))
          @foreach($users as $user)

          <a href="{{route('friend.profile', ['id' => $user->user_id])}}" class="list-group-item list-group-item-action flex-column align-items-start">
            <div class="row">
              <div class="col-md-1">
                <img style="width: 100%; height: 100%;" class="img-thumbnail" src="/user/{{$user->user_id}}/avatar">
              </div>
              <div class="col-md-9">
                <div >
                  <h5 class="mb-1"> {{$user->username}}</h5>
                </div>
              </div>
            </div>
            <p class="mb-1">
              {{$user->email}}
            </p>
          </a>


          @endforeach
          @endif

        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>
</body>
</html>


@endsection
