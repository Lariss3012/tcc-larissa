@php app()->setLocale(Auth::user()->lang); @endphp

@extends('layouts.app')

@section('content')

<style type="text/css">

.justify-content-center{
    margin-bottom: 30px;
}
.list-group-item{
    margin-bottom: 10px;
    margin-top: 10px;
    padding: 20px;
}
button{
    margin-left: 20px;
}
.gown{
    display: none;
}
.gpart{
    display: none;
}
</style>

<script type="text/javascript">
    $(document).ready(function(){
        $("#gpart").click(function(){
            $(".gpart").show();
            $(".gown").hide();
        });
        $("#gown").click(function(){
            $(".gpart").hide();
            $(".gown").show();
        });
    });
</script>
<div class="container">

 <div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
        <form class="form-inline md-form form-sm" method="get" action="{{route('searchgroup')}}">
            <input class="form-control" type="text" placeholder="Search" aria-label="Search" name="group">
            <button type="submit" class="btn btn-primary"><i class="fa fa-search" aria-hidden="true"></i> {{__('messages.searchgroup')}}</button>
        </form>

    </div>
    <div class="col-md-4">
        <a href="modal" data-toggle="modal" data-target="#modal"><button class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> {{__('messages.creategroup')}} </button></a>
    </div>
</div>

<div class="container">
    <nav aria-label="...">
      <ul class="pagination">
        <li class="page-item ">
          <a class="page-link" id="gpart" href="#"> {{__('messages.ingroup')}}</a>
      </li>
      <li class="page-item"><a class="page-link" id="gown" href="#">{{__('messages.owngroup')}}</a></li>

  </ul>
</nav>

<div class="row">
    <div class="col-md-12">
        <div class="list-group gpart">
            <h5>{{__('messages.ingroup')}}</h5>
            @if(isset($participant))
            @foreach($participant as $pa)
            @foreach($pa as $val)
            <a href="{{route('group.show', ['id' => $val->group_id])}}" class="list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-between">
                    <h5 class="mb-1">{{$val->name}}</h5>
                    <small>
                        @if($val->private)
                        <i class="fa fa-lock" aria-hidden="true"></i>
                        @endif
                        {{ date('d/m/Y', strtotime($val->created_at)) }}
                    </small>
                </div>
            </a>
            @endforeach
            @endforeach
            @endif
        </div>
    </div>

    <div class="col-md-12">
      <div class="list-group gown">
        <h5>{{__('messages.owngroup')}}</h5>
        @if(isset($owner))
        @foreach($owner as $val)
        <a href="{{route('group.show', ['id' => $val->group_id])}}" class="list-group-item list-group-item-action flex-column align-items-start">
            <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1">{{$val->name}}</h5>
                <small>
                    @if($val->private)
                    <i class="fa fa-lock" aria-hidden="true"></i>
                    @endif
                    {{$val->created_at}}
                </small>
            </div>
        </a>

@endforeach
@endif

</div>
</div>
</div>
</div>

<!-- Modal -->
<div class="modal fade" id="modal" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">{{__('messages.newgroup')}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
      </button>
  </div>
  <div class="modal-body">
    <form action="{{route('group.adding')}}" method="post">
        @csrf
        <div class="form-group">
            <h5>{{__('messages.namegroup')}}</h5>
            <input type="text" name="name"  required="true">
        </div>
        <div class="form-group">
            <h5>{{__('messages.descgroup')}} </h5>
            <textarea class="form-control" id="description" name="description" rows="3"></textarea>
        </div>
        <div class="form-group">
          <h5> {{__('messages.privategroup')}}</h5>
          <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="private" value="1" required="true">
              <label class="form-check-label" for="private">{{__('messages.yes')}}</label>
          </div>
          <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="private" value="0">
              <label class="form-check-label" for="private">{{__('messages.no')}}</label>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('messages.close')}}</button>
        <button type="submit" class="btn btn-primary">{{__('messages.ok')}} </button>
    </div>
</form>
</div>

</div>
</div>
</div>



</div>

@endsection

