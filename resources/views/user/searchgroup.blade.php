@php app()->setLocale(Auth::user()->lang); @endphp

@php
use App\User;
@endphp

@extends('layouts.app')

@section('content')

<style type="text/css">
.list-group-item{
  margin-bottom: 10px;
  margin-top: 10px;
  padding: 20px;
}
form{
  padding: 20px;
}
button{
  margin-left: 20px;
}
</style>

<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-10">
      <form class="form-inline md-form form-sm" method="get" action="{{route('searchgroup')}}">
        <input class="form-control" type="text" aria-label="Search" name="group">
        <button type="submit" class="btn btn-primary"><i class="fa fa-search" aria-hidden="true"></i> {{__('messages.search')}}</button>
      </form>
    </div>
  </div>

  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-10">
        @if(count($groups))
        <h3>{{__('messages.groups')}}</h3>

        <div class="list-group">

          @foreach($groups as $group)
          <div class="list-group-item list-group-item-action flex-column align-items-start">
            <div class="row">
              <div class="col-md-10">
                <div class="d-flex w-100 justify-content-between">
                  <h5 class="mb-1"> {{$group->name}}</h5>
                </div>
              </div>
              <div class="col-md-2">
                <a href="{{route('group.entering', [ 'group' => $group->group_id])}}"><button type="button" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> {{__('messages.enter')}}</button></a>
              </div>
            </div>
            <p class="mb-1">
              {{$group->description}}
            </p>
            @php $user = DB::table('users')->where('user_id', $group->user_id)->first(); @endphp
            <small> {{__('messages.owner')}} {{$user->username}} </small>
          </div>
        </div>
        @endforeach
        @else
        <h5> {{__('messages.notfound')}}</h5>
        @endif
      </div>
    </div>
  </div>
</body>
</html>


@endsection
