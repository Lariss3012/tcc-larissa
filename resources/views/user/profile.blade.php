@php app()->setLocale(Auth::user()->lang); @endphp

@extends('layouts.app')

@section('content')

<div class="alert alert-primary alert-dismissible fade show" role="alert">
  {{__('messages.please')}}<br>
  <a href="{{__('messages.linksats')}}"> {{__('messages.sendsats')}} <a>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>

<style type="text/css">

.card.bg{
    display: inline-block;
}
.card{
    margin-bottom: 32px;
}
div.col-sm-4{
    padding-left: 50px;
}
td{
    padding: 20px;
}
small{
    margin-right: 5px;
}
button.post{
    padding-left: 25px;
    padding-right: 25px;
}
h4{
    margin-left: 30px;
}
.n{
    margin-right: 15px;
}
.username{
    color: #0056b3;
}
.comment{
    display: none;
}
.desver{
    display: none;
}
.deslike{
    display: none;
}
</style>
@foreach($user_post as $p)
<script type="text/javascript">
    $(document).ready(function(){
        $("#ver{{$p->post_id}}").click(function(){
            $(".comment{{$p->post_id}}").show();
            $(".desver{{$p->post_id}}").show();
            $(".ver{{$p->post_id}}").hide();
        });
        $("#desver{{$p->post_id}}").click(function(){
            $(".desver{{$p->post_id}}").hide();
            $(".comment{{$p->post_id}}").hide();
            $(".ver{{$p->post_id}}").show();
        });
    });
</script>
@endforeach
<script type="text/javascript">
 function likepost(postid){
    $.get( "/home/user/like/"+postid);
    var likes = $('#likes-post-'+postid).text();
    document.getElementById("likes-post-"+postid).innerHTML = Number(likes) + 1 ;
    $("#deslike"+postid).show();
    $("#like"+postid).hide();
}

function deslikepost(postid){
    $.get( "/home/user/deslike/"+postid);
    var likes = $('#likes-post-'+postid).text();
    document.getElementById("likes-post-"+postid).innerHTML = Number(likes) - 1 ;
    $("#like"+postid).show();
    $("#deslike"+postid).hide();
}
</script>

<div class="row">
    <div class="col-sm-4">
        <div class="card bg-light mb-3" style="max-width: 18rem;">
            <div class="card-header"><h3><i class="fas fa-user"></i>  {{__('messages.aboutyou')}}</h3>
            </div>
            <div class="profile-header-img">
                <img style="width: 100%; height: 100%;" class="img-thumbnail" src="/user/{{$user->user_id}}/avatar" />
            </div>
            <div class="row justify-content-center">
                <form action="{{route('profile')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <input type="file" class="form-control-file" name="photo" id="avatarFile" aria-describedby="fileHelp">
                        <small id="fileHelp" class="form-text text-muted">{{__('messages.validimage')}}</small>
                    </div>
                    <button type="submit" class="btn btn-primary">{{__('messages.submit')}}</button>
                </form>
            </div>
            <div class="card-body">
                <h2 class="card-title">{{ $user->username }}</h2><br>
            </div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item">{{ $user->name }}</li>
                <li class="list-group-item">{{ $user->email }}</li>
            </ul>
        </div>


        <div class="card bg-light mb-3" style="max-width: 18rem;">
          <div class="card-header">{{__('messages.options')}}</div>
          <div class="card-body">
            <a href="{{ route('friends') }}"><h5 class="card-title">{{__('messages.friendships')}}</h5></a>
            <a href="{{route('records')}}"><h5 class="card-title">{{__('messages.records')}}</h5></a>
            <a href="{{ route('groups') }}"><h5 class="card-title">{{__('messages.groups')}}</h5></a>
            <a href="modal" data-toggle="modal" data-target="#modal-up"><h5 class="card-title">{{__('messages.editprofile')}}</h5></a>
        </div>
    </div>
</div>

<div class="col-sm-8">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route('post.adding') }}" method="post" >
                            {{csrf_field()}}
                            <label for="text"><h4>Post</h4></label>
                            <div class="form-group row post">
                                <div class="col-sm-12">

                                    <textarea class="form-control" id="content" name="content" rows="5"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">

                                <div class="col-sm-9">
                                    <label for="text"><small>Tags</small></label>
                                    <input type="text"  id="input-tags" name="tags" ></input>
                                    <script>
                                        $('#input-tags').selectize({
                                            delimiter: ', ',
                                            persist: false,
                                            create: function(input) {
                                                return {
                                                    value: input,
                                                    text: input
                                                }
                                            }
                                        });
                                    </script>
                                </div>
                                <div class="col-sm-3">
                                    <br><div class="custom-control custom-checkbox">

                                        <input type="checkbox" class="custom-control-input" id="customCheck1" name="quote" value="1"><label class="custom-control-label" for="customCheck1" ><small>{{__('messages.existquote')}}</small></label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary float-right post">{{__('messages.post')}}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        @foreach($user_post as $p)
        <div class="row ">
            <div class="col-md-9">
                <div class="card" >
                  <div class="card-body">
                    <div class="row">
                        <div class="col-md-10">
                            <h5 class="card-title">{{$user->username}}</h5>
                        </div>
                        <div class="col-md-2">
                            <a href="{{route('post.deleting', ['id' => $p->post_id])}}"><i class="fa fa-trash" aria-hidden="true"></i></a>
                        </div>
                    </div>
                    <h6 class="card-subtitle mb-2 text-muted">{{__('messages.created')}}: {{ date('d/m/Y', strtotime($p->created_at)) }}</h6>
                    <p class="card-text">{!!$p->content!!}</p>
                    @if($p->quote)
                    <small><strong>{{__('messages.quote')}}</strong></small><br>
                    @endif
                    <small>Tags: {{$p->tags}}</small><br>
                    @php $cont = count(DB::table('posts_users')->where('post_id',$p->post_id)->get()); @endphp
                    <a href="modal" data-toggle="modal" data-target="#modal{{$p->post_id}}" class="card-link n" id="likes-post-{{$p->post_id}}">{{$cont}}</a>
                    <a href="#!" class="like{{$p->post_id}} like" id="like{{$p->post_id}}" onClick="likepost({{$p->post_id}})"><i class="fa fa-thumbs-up" id="like-icon{{$p->post_id}}"></i> {{__('messages.like')}}</a>

                    <a href="#!" class="deslike{{$p->post_id}} deslike" id="deslike{{$p->post_id}}" onClick="deslikepost({{$p->post_id}})"><i class="fa fa-thumbs-down" id="deslike-icon{{$p->post_id}}"></i> {{__('messages.dislike')}}</a>
                </div>
                <form action="{{route('comment.adding', ['post' => $p->post_id])}}" method="post">
                 {{csrf_field()}}
                 <div class="input-group">
                    <input class="form-control" placeholder="{{__('messages.addcomment')}}" type="text" name="comment" required>
                    <span class="input-group-addon">
                        <input type="submit" value="{{__('messages.comment')}}" class="btn btn-primary" >
                    </span>
                </div>
            </form>
            <ul class="list-group list-group-flush ver{{$p->post_id}}" id="ver{{$p->post_id}}">
                <li class="list-group-item">
                    <a href="#!"> {{__('messages.seecomment')}}</a>
                </li>
            </ul>
            <ul class="list-group list-group-flush comment comment{{$p->post_id}}">
                @foreach($comments as $comment)
                @foreach($comment as $co)
                @if($co->post_id == $p->post_id)
                <li class="list-group-item">
                    @php $user_co= App\User::where('user_id',$co->user_id)->first(); @endphp
                    <span class="username">{{$user_co->username}}</span>
                    <br>{{$co->content}} </li>
                    @endif
                    @endforeach
                    @endforeach
                </ul>
                <ul class="list-group list-group-flush desver desver{{$p->post_id}}" id="desver{{$p->post_id}}">
                    <li class="list-group-item">
                        <a href="#!"> {{__('messages.closecomment')}}</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modal{{$p->post_id}}" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">{{__('messages.likes')}}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
          </button>
      </div>
      <div class="modal-body">
        @foreach($likes as $li)
        @if($li->post_id == $p->post_id)
        <p>{{$li->username}}</p>
        @if($li->user_id == Auth::user()->user_id)
        <script type="text/javascript">
            $("#like{{$p->post_id}}").hide();
            $("#deslike{{$p->post_id}}").show();
        </script>
        @endif
        @endif
        @endforeach
        <span>  </span>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('messages.close')}}</button>
        </div>
    </div>
</div>
</div>
</div>
@endforeach
</div>
</div>


</div>

</div>
</div>

<!-- Modal -->
<div class="modal fade" id="modal-up" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">{{__('messages.editprofile')}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
      </button>
  </div>
  <div class="modal-body">
    <form method="POST" action="{{ route('profile.updating') }}" >
        @csrf

        <div class="form-group row">
            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('messages.name') }}</label>

            <div class="col-md-6">
                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ $user->name }}" required autofocus>

                @if ($errors->has('name'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <label for="username" class="col-md-4 col-form-label text-md-right">{{ __('messages.username') }}</label>

            <div class="col-md-6">
                <input id="username" type="text" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{ $user->username }}" required autofocus>

                @if ($errors->has('username'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('username') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('messages.email') }}</label>

            <div class="col-md-6">
                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $user->email }}" required>

                @if ($errors->has('email'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <br>
        <small><strong> {{__('messages.alterpassword')}}</strong> </small>
        <div class="form-group row">

            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('messages.password') }}</label>

            <div class="col-md-6">
                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" >

                @if ($errors->has('password'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('messages.confirmpassword') }}</label>

            <div class="col-md-6">
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" >
            </div>
        </div>
        <br>
        <small><strong> {{__('messages.alterlang')}}</strong> </small>
        <div class="form-group row">
            <label for="language" class="col-md-4 col-form-label text-md-right">{{ __('messages.selectlang') }}</label>

            <div class="col-md-6">
                PT <input id="lang" type="radio" class="form-control{{ $errors->has('lang') ? ' is-invalid' : '' }}" name="lang" value="pt" autofocus>
                EN <input id="lang-en" type="radio" class="form-control{{ $errors->has('lang') ? ' is-invalid' : '' }}" name="lang" value="en" autofocus>

            </div>
        </div>

        <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
                <button type="submit" class="btn btn-primary">
                    {{ __('messages.submit') }}
                </button>
            </div>
        </div>
    </form>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('messages.close')}}</button>
    </div>
</div>
</div>
</div>
</div>



@endsection
