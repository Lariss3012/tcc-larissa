@php app()->setLocale(Auth::user()->lang); @endphp

@extends('layouts.app')

@section('content')

<div class="alert alert-primary alert-dismissible fade show" role="alert">
  {{__('messages.please')}}<br>
  <a href="{{__('messages.linksats')}}"> {{__('messages.sendsats')}} <a>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>

<style type="text/css">

div.col-md-12{

    padding-bottom: 20px;
}
.card{
    margin-bottom: 10px;
}
small{
    color: gray;
}
.n{
    margin-right: 15px;
}
.username{
    color: #0056b3;
}
.comment{
    display: none;
}
.desver{
    display: none;
}
.deslike{
    display: none;
}
</style>
@foreach($list_feed as $p)
<script type="text/javascript">
    $(document).ready(function(){
        $("#ver{{$p->post_id}}").click(function(){
            $(".comment{{$p->post_id}}").show();
            $(".desver{{$p->post_id}}").show();
            $(".ver{{$p->post_id}}").hide();
        });
        $("#desver{{$p->post_id}}").click(function(){
            $(".desver{{$p->post_id}}").hide();
            $(".comment{{$p->post_id}}").hide();
            $(".ver{{$p->post_id}}").show();
        });
    });
</script>
@endforeach
<script type="text/javascript">
   function likepost(postid){
    $.get( "/home/user/like/"+postid);
    var likes = $('#likes-post-'+postid).text();
    document.getElementById("likes-post-"+postid).innerHTML = Number(likes) + 1 ;
    $("#deslike"+postid).show();
    $("#like"+postid).hide();
}

function deslikepost(postid){
    $.get( "/home/user/deslike/"+postid);
    var likes = $('#likes-post-'+postid).text();
    document.getElementById("likes-post-"+postid).innerHTML = Number(likes) - 1 ;
    $("#like"+postid).show();
    $("#deslike"+postid).hide();
}
</script>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Feed</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    <div class="container">
                        @foreach($list_feed as $p)
                        @php $user = App\User::where('user_id',$p->user_id)->first();
                        @endphp
                        <div class="row ">
                            <div class="col-md-12">
                                <div class="card">
                                  <div class="card-body">
                                    <h5 class="card-title">{{$user->username}}</h5>
                                    <h6 class="card-subtitle mb-2 text-muted">{{__('messages.created')}} : {{ date('d/m/Y', strtotime($p->created_at)) }}</h6>
                                    <p class="card-text">{!!$p->content!!}</p>
                                    @if($p->quote)
                                    <small><strong>{{__('messages.quote')}}</strong></small><br>
                                    @endif
                                    <small>Tags: {{$p->tags}}</small><br>
                                    @php $cont = count(DB::table('posts_users')->where('post_id',$p->post_id)->get()); @endphp
                                    <a href="modal" data-toggle="modal" data-target="#modal{{$p->post_id}}" class="card-link n" id="likes-post-{{$p->post_id}}">{{$cont}}</a>

                                    <a href="#!" class="like{{$p->post_id}} like" id="like{{$p->post_id}}" onClick="likepost({{$p->post_id}})"><i class="fa fa-thumbs-up" id="like-icon{{$p->post_id}}"></i> {{__('messages.like')}}</a>

                                    <a href="#!" class="deslike{{$p->post_id}} deslike" id="deslike{{$p->post_id}}" onClick="deslikepost({{$p->post_id}})"><i class="fa fa-thumbs-down" id="deslike-icon{{$p->post_id}}"></i> {{__('messages.dislike')}}</a>
                                </div>
                                <form action="{{route('comment.adding', ['post' => $p->post_id])}}" method="post">
                                 {{csrf_field()}}
                                 <div class="input-group">
                                    <input class="form-control" placeholder="{{__('messages.addcomment')}}" type="text" name="comment" required>
                                    <span class="input-group-addon">
                                        <input type="submit" value="{{__('messages.comment')}}" class="btn btn-primary" >
                                    </span>
                                </div>
                            </form>
                            <ul class="list-group list-group-flush ver{{$p->post_id}}" id="ver{{$p->post_id}}">
                                <li class="list-group-item">
                                    <a href="#!"> {{__('messages.seecomment')}}</a>
                                </li>
                            </ul>
                            <ul class="list-group list-group-flush comment comment{{$p->post_id}}">
                                @foreach($comments as $comment)
                                @foreach($comment as $co)
                                @if($co->post_id == $p->post_id)
                                <li class="list-group-item">
                                    @php $user_co = App\User::where('user_id',$co->user_id)->first(); @endphp
                                    <span class="username">{{$user_co->username}}</span>
                                    <br>{{$co->content}} </li>
                                    @endif
                                    @endforeach
                                    @endforeach
                                </ul>
                                <ul class="list-group list-group-flush desver desver{{$p->post_id}}" id="desver{{$p->post_id}}">
                                    <li class="list-group-item">
                                        <a href="#!"> {{__('messages.closecomment')}}</a>
                                    </li>
                                </ul>
                            </div>

                        </div>
                    </div>

                    <!-- Modal -->
                    <div class="modal fade" id="modal{{$p->post_id}}" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                      <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">{{__('messages.likes')}}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                          </button>
                      </div>
                      <div class="modal-body">
                        @foreach($likes as $li)
                        @if($li->post_id == $p->post_id)
                        <p>{{$li->username}}</p>
                        @if($li->user_id == Auth::user()->user_id)
                        <script type="text/javascript">
                            $("#like{{$p->post_id}}").hide();
                            $("#deslike{{$p->post_id}}").show();
                        </script>
                        @endif
                        @endif
                        @endforeach
                        <span>  </span>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('messages.close')}}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
</div>
</div>
</div>
</div>
@endsection
