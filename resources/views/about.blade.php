
@extends('layouts.app')

@section('content')

<style type="text/css">
    .jumbotron{
        margin-top: 20px;
        margin-left: 40px;
        margin-right:40px;
    }
</style>

@if(Auth::user())
 @php app()->setLocale(Auth::user()->lang); @endphp

<div class="jumbotron">
  <h1 class="display-4">{{__('messages.hello')}}</h1>
  <p class="lead">{{__('messages.website')}}</p>
  <hr class="my-4">
  <p>{{__('messages.questions')}}</p>
  <a href="{{__('messages.linkquest')}}">{{__('messages.sendquestion')}}</a><p></p>
  <p>{{__('messages.please')}}</p>
  <a href="{{__('messages.linksats')}}">{{__('messages.sendsats')}}</a>
</div>

@else

<div class="jumbotron">
  <h1 class="display-4">Hello!</h1>
  <p class="lead">My name is Larissa and I made this website as a way to help people who like books and movies, its a social media just for us.</p>
  <hr class="my-4">
  <p>If you have any questions please submit then to the form in the link below with your email and I will answer as quickly as I can.</p>
  <a href="https://goo.gl/forms/BuzvQc2q7ArCqP4r2"> Send Your questions here.</a><p></p>
  <a href="https://goo.gl/forms/MxnfzqCBNWg8Ydw52"> Mande suas perguntas aqui.</a><p></p>
  <p>Please after using the website help me by answering the satisfaction form and giving some opinions, no identification required on this one :) .</p>
  <a href="https://goo.gl/forms/9xXSUhXyzERqyoQt2"> Answer the satisfaction form here!</a><p></p>
  <a href="https://goo.gl/forms/QsSI13EyndlYzLgB2"> Responda o questionário de satisfação aqui!</a>
</div>

@endif
@endsection
