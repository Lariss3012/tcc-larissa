<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/about',function () {
    return view('about');
})->name('about');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('user/{id}/avatar', function ($id) {
    // Find the user
    $user = App\User::findOrFail($id);

    // Return the image in the response with the correct MIME type
    return response()->make($user->photo, 200, array(
        'Content-Type' => (new finfo(FILEINFO_MIME))->buffer($user->photo)
    ));
});

Route::get('/home/friends', 'UserController@myFriends')->name('friends');
Route::get('/home/friends/adding/{id}', 'UserController@getAddFriend')->name('friend.adding');
Route::get('/home/friends/deleting/{id}', 'UserController@getRemoveFriend')->name('friend.removing');
Route::get('/home/profile/{id}','UserController@friendProfile')->name('friend.profile');
Route::get('/home/profile', 'UserController@index')->name('profile');
Route::post('/home/profile', 'UserController@updateAvatar');
Route::post('/home/profile/update','UserController@profileUpdate')->name('profile.updating');

Route::post('/home/profile/post', 'PostController@addPost')->name('post.adding');
Route::get('/home/profile/post/{id}', 'PostController@deletePost')->name('post.deleting');

Route::get('/home/search','UserController@search' )->name('search');

Route::get('/home/searchbook','BookController@searchBooks')->name('searchbook');
Route::post('/home/book/adding/{book}', 'BookController@addBook')->name('book.adding');

Route::get('/home/searchmovie','MovieController@searchMovies')->name('searchmovie');
Route::post('/home/movie/adding/{movie}', 'MovieController@addMovie')->name('movie.adding');

Route::get('/home/searchtag', 'UserController@searchTag')->name('searchtag');
Route::get('/home/searchuser', 'UserController@searchUser')->name('searchuser');

Route::get('/home/searchgroup', 'GroupController@searchGroup')->name('searchgroup');

Route::get('/home/user/records', 'RecordController@index')->name('records');
Route::get('/home/friend/records/{friend}','RecordController@friendRecords')->name('friend.records');
Route::get('/home/friend/records/public/{person}','RecordController@publicRecords')->name('public.records');
Route::get('/home/records/{record}', 'RecordController@deleteRecord')->name('record.deleting');

Route::get('/home/user/groups', 'GroupController@index')->name('groups');
Route::post('/home/user/group', 'GroupController@addGroup')->name('group.adding');
Route::get('/home/user/group/{id}', 'GroupController@groupShow')->name('group.show');
Route::get('/home/user/group/enter/{group}', 'GroupController@enterGroup')->name('group.entering');
Route::post('/home/user/group/{group}', 'GroupController@addParticipant')->name('participant.adding');
Route::get('/home/user/group/out/{group}', 'GroupController@deleteParticipant')->name('participant.deleting');
Route::get('/home/user/group/delete/{group}', 'GroupController@deleteGroup')->name('group.deleting');

Route::post('/home/user/comment/{post}', 'PostController@addComment')->name('comment.adding');

Route::get('/home/user/like/{id}', 'PostController@like')->name('liking');
Route::get('/home/user/deslike/{id}', 'PostController@deslike')->name('desliking');
