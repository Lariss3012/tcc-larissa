<?php

use Illuminate\Database\Seeder;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* $user = new \App\User();
        $user->create([
            'name' => 'Larissa',
             'email'=> 'lariss3012@gmail.com',
             'password' => bcrypt('123')
         ]);*/

         $tag = new \App\Tag();
         $tag->create([

            'tag' => 'Action'

        ]);

         $tag = new \App\Tag();
         $tag->create([

            'tag' => 'Adventure'

        ]);

         $tag = new \App\Tag();
         $tag->create([

            'tag' => 'Comedy'

        ]);

         $tag = new \App\Tag();
         $tag->create([

            'tag' => 'Mystery'

        ]);

         $tag = new \App\Tag();
         $tag->create([

            'tag' => 'Drama'

        ]);

         $tag = new \App\Tag();
         $tag->create([

            'tag' => 'Fantasy'

        ]);

         $tag = new \App\Tag();
         $tag->create([

            'tag' => 'Historical'

        ]);

         $tag = new \App\Tag();
         $tag->create([

            'tag' => 'Historical Fiction'

        ]);

         $tag = new \App\Tag();
         $tag->create([

            'tag' => 'Biography'

        ]);

         $tag = new \App\Tag();
         $tag->create([

            'tag' => 'Horror'

        ]);

         $tag = new \App\Tag();
         $tag->create([

            'tag' => 'Romance'

        ]);

         $tag = new \App\Tag();
         $tag->create([

            'tag' => 'Science fiction'

        ]);

         $tag = new \App\Tag();
         $tag->create([

            'tag' => 'Religious'

        ]);
     }
 }
