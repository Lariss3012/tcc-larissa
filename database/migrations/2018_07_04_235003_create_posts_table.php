<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('post_id');
            $table->integer('user_id')->unsigned();
            $table->integer('group_id')->unsigned()->nullable();
            $table->foreign('user_id')
                ->references('user_id')->on('users')
                ->onDelete('cascade');
            $table->foreign('group_id')->nullable()
                ->references('group_id')->on('groups')
                ->onDelete('cascade');
            $table->string('book_id')->nullable();
            $table->foreign('book_id')
                ->references('book_id')->on('books')
                ->onDelete('cascade');
            $table->string('movie_id')->nullable();
            $table->foreign('movie_id')
                ->references('movie_id')->on('movies')
                ->onDelete('cascade');
            $table->text('tags')->nullable();
            $table->longText('content');
            $table->boolean('quote')->default(false);
            //sem data de postagem pois ja temos o created_at/update_at no timestamps
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
