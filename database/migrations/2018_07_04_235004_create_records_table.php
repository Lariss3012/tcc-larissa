<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('records', function (Blueprint $table) {
            $table->increments('record_id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
                ->references('user_id')->on('users')
                ->onDelete('cascade');
            $table->string('book_id')->nullable();
            $table->foreign('book_id')
                ->references('book_id')->on('books')
                ->onDelete('cascade');
            $table->string('book_name')->nullable();
            $table->string('movie_id')->nullable();
            $table->foreign('movie_id')
                ->references('movie_id')->on('movies')
                ->onDelete('cascade');
            $table->string('movie_name')->nullable();
            $table->date('conclusion_date')->nullable();
            $table->boolean('private')->default(false);
            $table->boolean('liked')->default(false);
            $table->text('tags')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('records');
    }
}
