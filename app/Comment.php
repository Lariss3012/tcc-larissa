<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $primaryKey = 'comment_id';

    protected $fillable = [
        'content',
    ];

    public function posts(){

        return $this->belongsTo('Post');
    }

    public function users(){
        return $this->belongsTo('User');
    }

}
