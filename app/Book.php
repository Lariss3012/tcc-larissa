<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $primaryKey = 'book_id';

    protected $fillable = [
        'author', 'title', 'escription', 'publication_date', 'publisher',
    ];


    public function records(){

        return $this->belongsTo('Record');
    }

}
