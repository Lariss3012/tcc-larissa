<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Record extends Model
{
    protected $primaryKey = 'record_id';

    protected $fillable = [
        'conclusion_date', 'private', 'liked',
    ];

    public function users(){

        return $this->belongsTo('User');
    }

    public function books(){

        return $this->hasOne('Book');
    }

    public function movies(){

        return $this->hasOne('Movie');
    }


}
