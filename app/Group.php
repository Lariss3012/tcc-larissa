<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $primaryKey = 'group_id';

    protected $fillable = [
        'name', 'description', 'private',
    ];


    public function users(){

        return $this->hasOne('User');
    }
    public function participants()
    {
        return $this->belongsToMany('Group', 'groups_users', 'group_id', 'user_id');
    }

    public function addParticipant(User $user)
    {
        $this->participants()->attach($user->id);
    }

    public function removeParticipant(User $user)
    {
        $this->participants()->detach($user->id);
    }

    public function posts(){

        return $this->hasMany('Post');
    }

}
