<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'users';

    protected $primaryKey = 'user_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'email', 'password', 'lang', 'photo'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function friends()
    {
        return $this->belongsToMany('App\User', 'friends_users', 'user_id', 'friend_id');
    }

    public function addFriend(User $user)
    {
        $this->friends()->attach($user);
    }

    public function removeFriend(User $user)
    {
        $this->friends()->detach($user);
    }

    public function groups()
    {
        return $this->hasMany('Group');
    }

    public function posts(){

        return $this->hasMany('App\Post', 'post_id');
    }

     public function comments()
    {
        return $this->hasMany('Comment');
    }

    public function records(){

        return $this->hasMany('App\Record', 'record_id');
    }

}
