<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{

    protected $table = 'posts';

    protected $primaryKey = 'post_id';

    protected $fillable = [
        'content',
    ];

    public function groups(){

        return $this->belongsTo('Groups');
    }

    public function users(){

        return $this->belongsTo('App\User', 'user_id');
    }

    public function comments()
    {
        return $this->hasMany('Comment');
    }

    public function books(){

        return $this->hasOne('Book');
    }

    public function movies(){

        return $this->hasOne('Movie');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Post', 'posts_tags', 'post_id', 'tag');
    }

    public function curtidas()
    {
        return $this->belongsToMany('App\Post', 'posts_users', 'post_id', 'user_id');
    }

    public function addCurtida(User $user)
    {
        $this->curtidas()->attach($user->id);
    }

    public function removeCurtida(User $user)
    {
        $this->curtidas()->detach($user->id);
    }
}
