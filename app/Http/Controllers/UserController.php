<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\User;
use App\Post;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class UserController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){

        $user = User::findOrFail(Auth::user('id'))->first();
        $user_post = DB::table('posts')
        ->where('user_id','=', $user->user_id)
        ->where('group_id', '=', null)
        ->get();

        $user_post = $user_post->reverse();

        foreach($user_post as $po){

            $comment = DB::table('comments')
            ->where('post_id', $po->post_id)
            ->get();

            $comments[] = $comment;

            $likes = DB::table('users')
            ->join('posts_users', 'users.user_id', '=', 'posts_users.user_id')
            ->select('users.*', 'posts_users.post_id')
            ->get();
        }

        return view('user.profile',compact('user', 'user_post','comments','likes'));
    }

    public function search(){
        return view('user.search');
    }

    public function searchTag(){
        $search = \Request::get('tag');

        $posts = DB::table('posts')
        ->where('tags', 'like', '%'.$search.'%')
        ->where('group_id', null)
        ->orderBy('created_at','desc')
        ->get();

        $records = DB::table('records')
        ->where('tags', 'like','%'.$search.'%')
        ->where('private', 0)
        ->get();

        return view('user.searchtags', compact('posts', 'records'));

    }

    public function searchUser(){
        $search = \Request::get('user');

        $users = DB::table('users')
        ->where([
            ['username','like', '%'.$search.'%'],
            ['user_id','<>', Auth::user()->user_id],
        ])
        ->orWhere([
            ['email', 'like', '%'.$search.'%'],
            ['user_id','<>', Auth::user()->user_id],
        ])
        ->get();


        return view('user.searchuser', compact('users','friends'));

    }

    public function updateAvatar(Request $request){

        $request->validate([
            'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:3072',
        ]);

        $file = $request->file('photo');

    // Get the contents of the file
        $contents = $file->openFile()->fread($file->getSize());

    // Store the contents to the database
        $user = Auth::user();
        $user->photo = $contents;
        $user->save();

        return Redirect::back();

    }

    public function profileUpdate(Request $request){

        $dados = $request->all();

        $user = Auth::user();

        if($dados['name'] != $user->name){
         $request->validate([
            'name' => 'required|string|max:255',
        ]);
         $user->name = $dados['name'] ;
     }

     if($dados['username']  != $user->username){
         $request->validate([
            'username' => 'required|string|unique:users',
        ]);
         $user->username = $dados['username'] ;
     }

     if($dados['email']  != $user->email){
         $request->validate([
            'email' => 'required|string|email|max:255|unique:users',
        ]);
         $user->email = $dados['email'] ;
     }

     if ( ! $dados['password']  == null ){
        $request->validate([
            'password' => 'string|min:6|confirmed',
        ]);
        $user->password = bcrypt($dados['password'] );
    }

    if (isset($dados['lang'])) {

        $user->lang = $dados['lang'] ;
    }

    $user->save();

    return Redirect::back();

}

public function myFriends(){
    $user = User::findOrFail(Auth::user('id'));

    $friend = $user->first()->friends()->get();

    $folower = DB::table('friends_users')->where('friend_id', Auth::user()->user_id)->get();

    $list_friend = array();
    $list_follows = array();

    if(count($friend)){
        foreach($friend as $f)
        {
            $list_friend[] = User::where('user_id', $f->user_id)->first();
        }
    }

    if(count($folower)){
        foreach($folower as $f)
        {
            $list_follows[] = User::where('user_id', $f->user_id)->first();
        }
    }


    return view('user.friends', compact('list_friend', 'list_follows'));

}

public function getAddFriend($id){

    $user = User::findOrFail($id);
    Auth::user()->addFriend($user);
    return Redirect('/home/profile/'.$id);
}

public function getRemoveFriend($id){
  $user = User::find($id);
  Auth::user()->removeFriend($user);
  return Redirect::back();
}

public function friendProfile($id){

    $friend = User::findOrFail($id);

    $isfriend = DB::table('friends_users')
    ->where([
        ['user_id', Auth::user()->user_id],
        ['friend_id', $friend->user_id],
    ])->get();

    if(count($isfriend)){
        $sit = 1;
    }else{
        $sit = 0;
    }

    $friend_post = DB::table('posts')
    ->where('user_id','=', $friend->user_id)
    ->where('group_id', '=', null)
    ->get();

    $friend_post = $friend_post->reverse();

    foreach($friend_post as $po){

        $comment = DB::table('comments')
        ->where('post_id', $po->post_id)
        ->get();

        $comments[] = $comment;

        $likes = DB::table('users')
        ->join('posts_users', 'users.user_id', '=', 'posts_users.user_id')
        ->select('users.*', 'posts_users.post_id')
        ->get();
    }

    return view('user.friendProfile',compact('friend', 'friend_post','comments','likes','sit'));

}


}
