<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\User;
use App\Post;
use App\Group;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class GroupController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){

        $user = Auth::user();

        $owner = DB::table('groups')
        ->where('user_id','=',$user->user_id)
        ->orderBy('created_at', 'desc')
        ->get();

        $group = DB::table('groups_users')
        ->where('user_id', '=', $user->user_id)
        ->get();

        foreach($group as $g){

            $participant[] = DB::table('groups')
            ->where('group_id','=',$g->group_id)
            ->orderBy('created_at', 'desc')
            ->get();

        }

        return view('user.groups',compact('owner','participant'));
    }

    public function searchGroup(){
        $search = \Request::get('group');

        $part = DB::table('groups_users')
        ->where('user_id', '=', Auth::user()->user_id)
        ->get();

        if(count($part)){
            foreach($part as $pa){
                $groups = DB::table('groups')
                ->where([
                    ['user_id','<>', Auth::user()->user_id],
                    ['group_id','<>', $pa->group_id ],
                    ['name', 'like', '%'.$search.'%'],
                    ['private', 0],
                ])
                ->get();
            }
        }else{

            $groups = DB::table('groups')
            ->where([
                ['user_id','<>', Auth::user()->user_id ],
                ['name', 'like', '%'.$search.'%'],
                ['private', 0],
            ])
            ->get();

        }

        return view('user.searchgroup',compact('groups'));

    }

    public function addGroup(Request $request){

        $user = User::findOrFail(Auth::user('id'))->first();

        $group = new Group($request->all());
        $group->user_id = $user->user_id;
        $group->save();

        return Redirect::back();

    }

    public function groupShow($id){

        $posts = DB::table('posts')
        ->where('group_id', '=', $id)
        ->orderBy('created_at', 'desc')
        ->get();

        foreach($posts as $p){
            $comment = DB::table('comments')
            ->where('post_id', $p->post_id)
            ->get();

            $comments[] = $comment;

            $likes = DB::table('users')
            ->join('posts_users', 'users.user_id', '=', 'posts_users.user_id')
            ->select('users.*', 'posts_users.post_id')
            ->get();
        }

        $infos = DB::table('groups')
        ->where('group_id', '=', $id)
        ->first();

        $participants = DB::table('groups_users')
        ->where('group_id', '=', $id)
        ->get();

        $friends = Auth::user()->friends()->get();

        foreach($participants as $pa){
            $friends = Auth::user()->friends()->where('friend_id', '<>', $pa->user_id)->get();
        }

        if($infos->private && $infos->user_id == Auth::user()->user_id){
            $sit = 1;
        }
        elseif ($infos->private) {
            $sit = 0;
        }elseif($infos->user_id == Auth::user()->user_id){
            $sit= 2;
        }else{
            $sit=null;
        }

        return view('user.group',compact('posts','infos','participants','friends','comments','sit','likes'));
    }

    public function deleteGroup($group){

        $user = Auth::user('id');

        DB::table('groups_users')
        ->where('group_id', $group)
        ->delete();

        DB::table('groups')
        ->where('group_id', $group)
        ->delete();

        return Redirect('/home/user/groups');

    }

    public function enterGroup($group){

        DB::table('groups_users')->insert(
            ['group_id' => $group, 'user_id' => Auth::user('id')->user_id] );

        return Redirect('/home/user/groups');

    }

    public function addParticipant(Request $request, $group){


        $participant = $request->all();

        foreach($participant['participant'] as $part){
            DB::table('groups_users')->insert(
                ['group_id' => $group, 'user_id' => $part] );
        }

        return Redirect::back();
    }

    public function deleteParticipant($group){

        $user = Auth::user('id');

        Db::table('groups_users')
        ->where([
            ['user_id', $user->user_id ],
            ['group_id', $group],
        ])->delete();

        return Redirect('/home/user/groups');

    }
}
