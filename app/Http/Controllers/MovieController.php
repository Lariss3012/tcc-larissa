<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\User;
use App\Record;
use App\Movie;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class MovieController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function searchMovies(){

        $search = \Request::get('movie');

        $query = $search;

        $query = preg_replace('/\s+/','+',$query);

        $json = file_get_contents("http://www.omdbapi.com/?apikey=402b40bc&t=".$query."&type=movie&plot=full");

        $results = json_decode($json,true);

        $movies_bd = DB::table('movies')
        ->where('title', 'like', '%'.$search.'%')
        ->get();

        $movies_bd->toArray();

        return view('user.searchmovies',compact('results','movies_bd'));
    }

    public function addMovie(Request $request, $movie){

        $dados = $request->all();

        $query = $movie;

        $json = file_get_contents("http://www.omdbapi.com/?apikey=402b40bc&i=".$query);

        $results = json_decode($json,true);


        $user = User::findOrFail(Auth::user('id'))->first();

        $movie_bd = DB::table('movies')
        ->where('movie_id', '=', $results['imdbID'])
        ->first();


        if(isset($movie_bd)){

            $record = new Record();

            $record->user_id = $user->user_id;;
            $record->movie_id = $movie_bd->movie_id;
            $record->movie_name = $movie_bd->title;
            $record->conclusion_date = $dados['date'];
            $record->liked = $dados['liked'];
            $record->private = $dados['private'];
            if (isset($dados['tags'])) {
                $record->tags = $dados['tags'];
            }
            $record->save();



        }else{

            $movies = new Movie();

            $movies->movie_id = $results['imdbID'];
            $movies->title = $results['Title'];
            if(array_key_exists('Director',$results)){
                $movies->director = $results['Director'];}
            if(array_key_exists('Plot',$results)){
                $movies->description = $results['Plot'];}
            if(array_key_exists('Writer',$results)){
                $movies->screenwriter = $results['Writer'];}
            if(array_key_exists('Released',$results)){
                $movies->publication_date = $results['Released'];}
            if(array_key_exists('Production',$results)){
                $movies->producer = $results['Production'];}

            $movies->save();

            $record = new Record();

            $moviie = DB::table('movies')
            ->where('movie_id', '=', $results['imdbID'])
            ->first();

            $record->user_id = $user->user_id;
            $record->movie_id = $moviie->movie_id;
            $record->movie_name = $moviie->title;
            $record->conclusion_date = $dados['date'];
            $record->liked = $dados['liked'];
            $record->private = $dados['private'];
            if (isset($dados['tags'])) {
                $record->tags = $dados['tags'];
            }
            $record->save();

            }

             return redirect()->route('records');

        }

    }
