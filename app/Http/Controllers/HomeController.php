<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\User;
use App\Post;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::findOrFail(Auth::user('id'));

        $friend = $user->first()->friends()->get();

        $list_friend = array();
        $list_feed = array();

        $num = Auth::user()->friends->count();

        foreach($friend as $f){
            $list_friend[] = $f;

        }

        for($x=0; $x<$num; $x++)
        {
            $feed = Post::where('user_id',$list_friend[$x]->user_id)->where('group_id', '=', null)->get();

            foreach($feed as $fe){
                $list_feed[] = $fe;



                $comment = DB::table('comments')
                ->where('post_id', $fe->post_id)
                ->get();

                $comments[] = $comment;

            }

            $likes = DB::table('users')
            ->join('posts_users', 'users.user_id', '=', 'posts_users.user_id')
            ->select('users.*', 'posts_users.post_id')
            ->get();
        }

        rsort($list_feed, SORT_NATURAL | SORT_FLAG_CASE);



        return view('home', compact('list_friend', 'list_feed','comments','likes'));
    }


}
