<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\User;
use App\Record;
use App\Tag;
use App\Movie;
use App\Book;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class RecordController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){

       $user = User::findOrFail(Auth::user('id'))->first();

       $records_books = DB::table('records')
       ->where([
        ['user_id', '=', $user->user_id],
        ['book_id', '<>', null],
    ])
       ->orderBy('conclusion_date','desc')
       ->get();

       $records_movies = DB::table('records')
       ->where([
        ['user_id', '=', $user->user_id],
        ['movie_id', '<>', null],
    ])
       ->orderBy('conclusion_date','desc')
       ->get();



       return view('user.records',compact('records_books', 'records_movies'));
   }

   public function deleteRecord($record){
    DB::table('records')
    ->where('record_id', $record)
    ->delete();

    return Redirect::back();

   }

   public function friendRecords($friend){

    $records_books = DB::table('records')
    ->where([
        ['user_id', $friend],
        ['book_id', '<>', null],
    ])
    ->orderBy('conclusion_date','desc')
    ->get();

    $records_movies = DB::table('records')
    ->where([
        ['user_id',$friend],
        ['movie_id', '<>', null],
    ])
    ->orderBy('conclusion_date','desc')
    ->get();

    return view('user.records',compact('records_books', 'records_movies'));

}

public function publicRecords($person){

    $records_books = DB::table('records')
    ->where([
        ['user_id', $person],
        ['book_id', '<>', null],
        ['private', 0],
    ])
    ->orderBy('conclusion_date','desc')
    ->get();

    $records_movies = DB::table('records')
    ->where([
        ['user_id',$person],
        ['movie_id', '<>', null],
        ['private', 0],
    ])
    ->orderBy('conclusion_date','desc')
    ->get();

    return view('user.records',compact('records_books', 'records_movies'));

}


}
