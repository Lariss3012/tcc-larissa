<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\User;
use App\Post;
use App\Comment;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use App\Http\Requests\PostRequest;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function addPost(PostRequest $request){

        $dados = $request->all();

        $user = User::findOrFail(Auth::user('id'))->first();

        $post = new Post;

        $dados['content'] = strip_tags ($dados['content']);

        $dados['content'] = str_replace("\r\n", '<br>', $dados['content']);

        $post->content = $dados['content'];

        $post->users()->associate($user);

        if (isset($dados['group'])) {
            $post->group_id = $dados['group'];
        }

        if (isset($dados['tags'])) {
            $post->tags = $dados['tags'];
        }

        if (isset($dados['quote'])) {
            $post->quote = $dados['quote'];
        }

        $post->save();


        return Redirect::back();

    }

    public function deletePost($id){

        $user = User::findOrFail(Auth::user('id'))->first();

        $post = Post::findOrFail($id);

        DB::table('posts_users')->where('post_id', '=', $id)->delete();
        DB::table('posts')->where('post_id', '=', $id)->delete();

        return Redirect::back();

    }

    public function addComment(Request $request, $post){

        $dados = $request->all();

        $user = User::findOrFail(Auth::user('id'))->first();

        $comment = new Comment();

        $comment->user_id = $user->user_id;
        $comment->post_id = $post;
        $comment->content = $dados['comment'];
        $comment->save();

        return Redirect::back();


    }

    public function like($id){

        $user = Auth::user();

        DB::table('posts_users')
        ->insert(
            ['post_id' => $id, 'user_id' => $user->user_id]
        );


    }

    public function deslike($id){

        $user = Auth::user();

        DB::table('posts_users')
        ->where([
            ['post_id', $id],
            ['user_id', $user->user_id],
        ])->delete();

    }

}
