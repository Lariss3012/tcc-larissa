<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\User;
use App\Book;
use App\Record;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class BookController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function searchBooks(){

        $search = \Request::get('book');

        $query = '';

        $query .= $search;

        $query = preg_replace('/\s+/','+',$query);

        $json = file_get_contents("https://www.googleapis.com/books/v1/volumes?q=".$query."&printType=books&key=AIzaSyD89EQEosbEP7JO2y1LsX_LNgw8p6o5OZk");

        $results = json_decode($json,true);

        return view('user.searchbooks',compact('results'));
    }

    public function addBook(Request $request, $book){

        $dados = $request->all();

        $query = $book;

        $json = file_get_contents("https://www.googleapis.com/books/v1/volumes/".$query."?key=AIzaSyD89EQEosbEP7JO2y1LsX_LNgw8p6o5OZk");

        $results = json_decode($json,true);

        $user = User::findOrFail(Auth::user('id'))->first();

        $book_bd = DB::table('books')
        ->where('book_id', '=', $results['id'])
        ->first();


        if(isset($book_bd)){

            $record = new Record();

            $record->user_id = $user->user_id;;
            $record->book_id = $book_bd->book_id;
            $record->book_name = $book_bd->title;
            $record->conclusion_date = $dados['date'];
            $record->liked = $dados['liked'];
            $record->private = $dados['private'];
            if (isset($dados['tags'])) {
                $record->tags = $dados['tags'];
            }
            $record->save();



        }else{

            $books = new Book();

            $books->book_id = $results['id'];
            $books->title = $results['volumeInfo']['title'];
            if(array_key_exists('authors',$results['volumeInfo'])){
            $books->author = implode(", ", $results['volumeInfo']['authors']);}
            if(array_key_exists('description',$results['volumeInfo'])){
            $books->description = $results['volumeInfo']['description'];}
            if(array_key_exists('publisher',$results['volumeInfo'])){
            $books->publisher = $results['volumeInfo']['publisher'];}
            if(array_key_exists('publishedDate',$results['volumeInfo'])){
            $books->publication_date = $results['volumeInfo']['publishedDate'];}
            $books->save();

            $record = new Record();

            $bookie = DB::table('books')
            ->where('book_id', '=', $results['id'])
            ->first();

            $record->user_id = $user->user_id;;
            $record->book_id = $bookie->book_id;
            $record->book_name = $bookie->title;
            $record->conclusion_date = $dados['date'];
            $record->liked = $dados['liked'];
            $record->private = $dados['private'];
            if (isset($dados['tags'])) {
                $record->tags = $dados['tags'];
            }
            $record->save();

        }

        return redirect()->route('records');
    }
}
