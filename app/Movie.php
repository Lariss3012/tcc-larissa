<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    protected $primaryKey = 'movie_id';

    protected $fillable = [
        'title', 'publication_date', 'description', 'director', 'screenwriter', 'producer',
    ];

    public function records(){

        return $this->belongsTo('Record');
    }


}
